public class Ticket {
    int id;
    Owner owner;
    String ticketNumber;

    public Ticket(int id, Owner owner, String ticketNumber) {
        this.id = id;
        this.owner = owner;
        this.ticketNumber = ticketNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    @Override
    public String toString() {
        return "Ticket:"+id+
                "\n" + owner +
                "\nticketNumber:" + ticketNumber+"\n";
    }
}
