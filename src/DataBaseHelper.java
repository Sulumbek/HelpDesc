import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class DataBaseHelper {
    void SaveOwner(List owners){
        File file = new File("simple.db");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            for (int i = 0; i<owners.size();i++) {
                Owner owner = (Owner) owners.get(i);
                bufferedWriter.write(owner.toString());
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    List readOwner(){
        ArrayList arrayList = new ArrayList();
        File file = new File("simple.db");
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            int a = 0;
            String name = "";
            String surname = "";
            String str = "";
            ArrayList arrayListOwner = new ArrayList();
            while((str = bufferedReader.readLine()) != null){
                String [] classSplits = str.split("=");
                String [] stringSplits = classSplits[1].split(":");
                if(stringSplits[0].equalsIgnoreCase("id")){
                    a = Integer.parseInt(stringSplits[1]);
                }
                if(stringSplits[2].equalsIgnoreCase("name")){
                    name = stringSplits[3];
                }
                if(stringSplits[4].equalsIgnoreCase("surname")){
                    surname = stringSplits[5];
                }
                if(classSplits[0].equalsIgnoreCase("Owner")){
                    Owner owner = new Owner(a,name,surname);
                    arrayListOwner.add(owner);
                }


            }
            return arrayListOwner;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    return null;
    }

    void SaveTickets(List tickets){
        File file = new File("simpleTickets.db");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            for (int i = 0; i<tickets.size();i++) {
                Ticket ticket = (Ticket) tickets.get(i);
                bufferedWriter.write(ticket.toString());
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    List readTickets(){
        ArrayList arrayList = new ArrayList();
        File file = new File("simpleTickets.db");
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            int idTicket = 0;

            int a = 0;
            String name = "";
            String surname = "";
            String str = "";
            Owner owner = null;

            String ticketsNum ="";

            ArrayList arrayListTicket = new ArrayList();
            while((str = bufferedReader.readLine()) != null){
                String [] classSplits = str.split("=");
                if(classSplits[0].equalsIgnoreCase("Owner")) {
                    String[] stringSplits = classSplits[1].split(":");
                    if(stringSplits[0].equalsIgnoreCase("id")){
                        a = Integer.parseInt(stringSplits[1]);
                    }
                    if(stringSplits[2].equalsIgnoreCase("name")){
                        name = stringSplits[3];
                    }
                    if(stringSplits[4].equalsIgnoreCase("surname")){
                        surname = stringSplits[5];
                    }
                    if(classSplits[0].equalsIgnoreCase("Owner")){
                        owner = new Owner(a,name,surname);
                    }
                }

                String [] taskSplit = str.split(":");
                if(taskSplit[0].equalsIgnoreCase("Ticket")){
                    idTicket = Integer.parseInt(taskSplit[1]);
                }
                if(taskSplit[0].equalsIgnoreCase("ticketNumber")){
                    ticketsNum = taskSplit[1];
                }

                if(str.equals("")){
                    Ticket ticket = new Ticket(idTicket,owner,ticketsNum);
                    arrayListTicket.add(ticket);
                }

            }
            return arrayListTicket;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
